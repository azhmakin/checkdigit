import Foedselsnummer.Companion.calcChecksum

/**
 * Norwegian national identification number -- fødselsnummer.
 *
 * http://fnrinfo.no/
 *
 * Source: https://en.wikipedia.org/wiki/National_identity_number_(Norway)
 * Source: https://no.wikipedia.org/wiki/F%C3%B8dselsnummer
 *
 * Tallteori 5.3 - Fødselsnummer oppbygning
 * https://www.youtube.com/watch?v=F-Cvr1hvidU
 *      Sample: 290287-246(49)
 *
 * 11 digits
 *
 * d1 d2 m1 m2 y1 y2 i1 i2 i3 c1 c2
 */
class Foedselsnummer
{
    // k1 = 11 - ((3 × d1 + 7 × d2 + 6 × m1 + 1 × m2 + 8 × å1 + 9 × å2 + 4 × i1 + 5 × i2 + 2 × i3) mod 11),
    // k2 = 11 - ((5 × d1 + 4 × d2 + 3 × m1 + 2 × m2 + 7 × å1 + 6 × å2 + 5 × i1 + 4 × i2 + 3 × i3 + 2 × k1) mod 11).

    companion object {
        private val WEIGHTS_1 = arrayOf(3, 7, 6, 1, 8, 9, 4, 5, 2)
        private val WEIGHTS_2 = arrayOf(5, 4, 3, 2, 7, 6, 5, 4, 3, 2)

        fun calcChecksum(str: String): String
        {
            return try {
                val k1 = calcCheckDigit(str, WEIGHTS_1)
                val k2 = calcCheckDigit(str + k1, WEIGHTS_2)

                "" + k1 + "" + k2
            } catch (e: RuntimeException) {
                "XX"
            }
        }

        private fun calcCheckDigit(s: String, w: Array<Int>): Int
        {
            assert(s.length == w.size)

            var sum = 0

            for (i in s.indices)
            {
                sum += Character.getNumericValue(s[i]) * w[i]
            }

            val digit = 11 - sum % 11

            if (digit > 9) throw RuntimeException("Illegal sequence!")

            return digit
        }
    }
}

fun String.isValidFoedselsnummer(): Boolean
{
    return this == this.substring(0, 9) + calcChecksum(this.substring(0, 9))
}

fun main() {
    println(Foedselsnummer.calcChecksum("290287246"))
    println(Foedselsnummer.calcChecksum("200557432")) // i

    println(Foedselsnummer.calcChecksum("300838266")) // i

    println("30083826529".isValidFoedselsnummer())
    println("30083826629".isValidFoedselsnummer())
    println("30083826729".isValidFoedselsnummer())
}
