
// PESEL - Powszechny Elektroniczny System Ewidencji Ludności
// https://en.wikipedia.org/wiki/PESEL

object PESEL
{
    fun calculateCheckDigit(pesel: String): Char
    {
        //ABCDEFGHIJK
        //01234567890

        val k = intArrayOf(1, 3, 7, 9, 1, 3, 7, 9, 1, 3)

        var sum = 0

        for (i in 0..9)
        {
            sum += k[i] * Character.getNumericValue(pesel[i])
        }

        println("sum = ${sum}")

        return '0'
    }

    @JvmStatic
    fun main(args: Array<String>)
    {
        calculateCheckDigit("12345678901")
    }
}
