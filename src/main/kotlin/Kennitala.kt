/***
 * The Icelandic identification number (Icelandic: kennitala, abbreviated kt.)
 * https://en.wikipedia.org/wiki/Icelandic_identification_number
 */
class Kennitala {

    companion object {
        private val WEIGHTS = arrayOf(3, 2, 7, 6, 5, 4, 3, 2)

        fun calcCheckDigit(s: String): Int
        {
            var sum = 0

            for (i in WEIGHTS.indices)
            {
                sum += Character.getNumericValue(s[i]) * WEIGHTS[i]
            }

            sum %= 11

            val digit = if (sum == 0) 0 else (11 - sum)

            if (digit > 9) throw RuntimeException("Illegal sequence")

            return digit
        }
    }
}

fun main() {
    val c = Kennitala.calcCheckDigit("1201743399")
    println(c)

    //println(Kennitala.calcCheckDigit("9999992022"))
    println(Kennitala.calcCheckDigit("6502697649"))
    println(Kennitala.calcCheckDigit("2006730779"))
    println(Kennitala.calcCheckDigit("0903432039")) // Bobby Fischer???
    println(Kennitala.calcCheckDigit("3112669539")) // Official Sample?
    println(Kennitala.calcCheckDigit("1212121239")) // Official Sample?
}
