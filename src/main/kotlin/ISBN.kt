
class ISBN {
    companion object {
        fun isValid(isbn: String): Boolean {
            var index = 11
            var result = 0

            for (i in 0..12) {
                val c = isbn[i]

                val v: Int =
                    if (Character.isDigit(c)) {
                        c - '0'
                    } else if (c == 'X') {
                        10
                    } else {
                        continue
                    }

                index--

                result += v * index
            }

            result %= 11

            return result == 0
        }


        fun calculateCheckDigit(isbn: String): Char {
            var index = 11
            var result = 0

            for (i in 0..11) {
                val c = isbn[i]

                if (!Character.isDigit(c)) {
                    continue
                }

                index--

                val d = c - '0'

                result += d * index
            }

            result = 11 - result % 11

            return if (result == 11) '0' else if (result == 10) 'X' else ('0' + result)
        }
    }
}
