/**
 * Source: https://en.wikipedia.org/wiki/Verhoeff_algorithm
 */
class Verhoeff {
    companion object {

        private val D = arrayOf(
                arrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
                arrayOf(1, 2, 3, 4, 0, 6, 7, 8, 9, 5),
                arrayOf(2, 3, 4, 0, 1, 7, 8, 9, 5, 6),
                arrayOf(3, 4, 0, 1, 2, 8, 9, 5, 6, 7),
                arrayOf(4, 0, 1, 2, 3, 9, 5, 6, 7, 8),
                arrayOf(5, 9, 8, 7, 6, 0, 4, 3, 2, 1),
                arrayOf(6, 5, 9, 8, 7, 1, 0, 4, 3, 2),
                arrayOf(7, 6, 5, 9, 8, 2, 1, 0, 4, 3),
                arrayOf(8, 7, 6, 5, 9, 3, 2, 1, 0, 4),
                arrayOf(9, 8, 7, 6, 5, 4, 3, 2, 1, 0),
            )

        private val INV = arrayOf(0, 4, 3, 2, 1, 5, 6, 7, 8, 9)

        private val P = arrayOf(
                arrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
                arrayOf(1, 5, 7, 6, 2, 8, 3, 0, 9, 4),
                arrayOf(5, 8, 0, 3, 7, 9, 6, 1, 4, 2),
                arrayOf(8, 9, 1, 6, 0, 4, 3, 5, 2, 7),
                arrayOf(9, 4, 5, 3, 1, 2, 6, 8, 7, 0),
                arrayOf(4, 2, 8, 6, 5, 7, 3, 9, 0, 1),
                arrayOf(2, 7, 9, 3, 8, 0, 6, 4, 1, 5),
                arrayOf(7, 0, 4, 6, 9, 1, 3, 2, 5, 8),
            )

        fun calculateCheckDigit(str: String): Char
        {
            var sum = 0
            var count = 0

            for (index in str.length - 1 downTo 0)
            {
                val c = str[index]

                if (c.isWhitespace())
                {
                    continue
                }
                else if (!c.isDigit())
                {
                    throw RuntimeException("Non-numeric string")
                }

                val digit = Character.getNumericValue(c)

                sum = D[sum][ P[(count + 1) % 8][digit] ]

                count++
            }

            return '0' + INV[sum]
        }

        fun isValid(str: String): Boolean
        {
            var sum = 0
            var count = 0

            for (index in str.length - 1 downTo 0)
            {
                val c = str[index]

                if (c.isWhitespace())
                {
                    continue
                }
                else if (!c.isDigit())
                {
                    throw RuntimeException("Non-numeric string")
                }

                val digit = Character.getNumericValue(c)

                sum = D[sum][ P[count % 8][digit] ]

                count++
            }

            return sum == 0
        }
    }
}


fun main()
{
    println(Verhoeff.isValid("23634"))
    println(Verhoeff.calculateCheckDigit("2363"))
}