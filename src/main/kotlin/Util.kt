import kotlin.random.Random

class Util {
    companion object {
        fun randomDigits(num: Int): String
        {
            val sb = StringBuilder()

            for (i in 1..num)
            {
                sb.append('0' + Random.nextInt(0, 10))
            }

            return sb.toString()
        }
    }
}
