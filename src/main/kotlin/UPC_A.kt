
object UPC_A
{
    fun calc(code: String) : Int
    {
        var oddSum = 0

        for (i in 0..10 step 2)
        {
            oddSum += code[i] - '0'
        }

        var evenSum = 0

        for (i in 1..9 step 2)
        {
            evenSum += code[i] - '0'
        }

        val temp = (oddSum * 3 + evenSum) % 10

        return if (temp == 0) 0 else (10 - temp)
    }


    fun isValid(code: String) : Boolean
    {
         return calc(code.substring(0, 11)) == (code[11] - '0')
    }


    @JvmStatic
    fun main(args: Array<String>)
    {
        println(isValid("036000241457"))
        //            123456789012

        println(calc("01010101010"))
    }
}
