/**
 * Jedinstveni matični broj građana / Јединствени матични број грађана
 * https://en.wikipedia.org/wiki/Unique_Master_Citizen_Number
 */
class JMBG
{
    companion object {
        private val index = intArrayOf(0, 6, 1, 7, 2, 8, 3, 9, 4, 10, 5, 11)

        fun calcCheckDigit(s: String): Int
        {
            var sum = 0

            for (i in index.indices)
            {
                val weight = 7 - i / 2

                sum += weight * (s[index[i]] - '0')
            }

            sum = 11 - sum % 11

            return if (sum > 9) 0 else sum
        }

        fun isValid(jmbg: String) : Boolean
        {
            return calcCheckDigit(jmbg) == jmbg[12] - '0'
        }
    }
}

// 0123456789012
// 0101006500006
// 0101100710006

fun main()
{
    val jmbg = "0101006500006"
    println(JMBG.calcCheckDigit(jmbg))
    println(JMBG.calcCheckDigit("0101100710006"))

    println(JMBG.isValid("0101006500006"))
    println(JMBG.isValid("0101100710006"))
}
